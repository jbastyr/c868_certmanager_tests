﻿using CertManager.Domain.Models;
using CertManager.Domain.Repositories;
using CertManager.Services;
using CertManagerTests.Mock.Repositories;
using System;
using System.Collections.Generic;
using Xunit;

namespace CertManagerTests.Tests.Services
{
    public class CertificateService_Export_Should
    {
        ICertificateRepository _repository;
        IWorkUnit _workUnit;
        CertificateService _service;
        List<Certificate> _certificates;

        public CertificateService_Export_Should()
        {
            _repository = new MockCertificateRepository();
            _workUnit = new MockWorkUnit();
            _service = new CertificateService(_repository, _workUnit);
        }

        [Fact]
        public async void ExportAll()
        {
            // arrange
            _certificates = await _service.ListAsync() as List<Certificate>;

            // act
            string csvList = _service.ExportCertificatesToCsv(_certificates);
            List<Certificate> csvCertificates = CsvToCertificateList(csvList);

            // assert
            Assert.True(csvCertificates.Count > 0);
            Assert.Equal(_certificates.Count, csvCertificates.Count);
            foreach (Certificate certificate in _certificates)
            {
                Assert.Contains(csvCertificates, (csvCertificate) => 
                    csvCertificate.Name == certificate.Name);
            }
        }

        [Fact]
        public void ExportNone()
        {
            // arrange
            _certificates = null;

            // act
            string csvList = _service.ExportCertificatesToCsv(_certificates);
            List<Certificate> csvCertificates = CsvToCertificateList(csvList);

            // assert
            Assert.True(csvCertificates.Count == 0);

        }

        private List<Certificate> CsvToCertificateList(string csvList)
        {
            List<Certificate> csvCertificates = new List<Certificate>();
            foreach (string line in csvList.Split("\r\n"))
            {
                // skip title and header
                if (line.StartsWith("Certificate Report") || line.StartsWith("Id")  || string.IsNullOrWhiteSpace(line))
                {
                    continue;
                }
                csvCertificates.Add(CertificateFromCsvLine(line));
            }
            return csvCertificates;
        }

        private Certificate CertificateFromCsvLine(string line)
        {
            string[] split = line.Split(',');
            return new Certificate()
            {
                Id = Convert.ToInt64(split[0]),
                Name = split[1],
                CreatedAt = DateTime.Parse(split[2]),
                ExpiresAt = DateTime.Parse(split[3]),
                AssetId = Convert.ToInt64(split[4])
            };
        }
        
    }
}
