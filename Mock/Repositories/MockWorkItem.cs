﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using CertManager.Domain.Repositories;


namespace CertManagerTests.Mock.Repositories
{
    class MockWorkUnit : IWorkUnit
    {
        public async Task CompleteAsync()
        {
            await Task.Run(() => true);
        }
    }
}
