﻿using CertManager.Domain.Models;
using CertManager.Domain.Repositories;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace CertManagerTests.Mock.Repositories
{
    class MockCertificateRepository : ICertificateRepository
    {
        private readonly List<Certificate> _certificates;

        public MockCertificateRepository()
        {
            _certificates = new List<Certificate>()
            {
                new Certificate() { Id = 1, Name = "Just Created", CreatedAt = DateTime.Now, AssetId = 1 },
                new Certificate() { Id = 2, Name = "Expired", CreatedAt = DateTime.Now, ExpiresAt = DateTime.Now.AddDays(-1), AssetId = 1 },
                new Certificate() { Id = 3, Name = "Expiring Soon", CreatedAt = DateTime.Now, ExpiresAt = DateTime.Now.AddDays(2), AssetId = 2 }
            };
        }

        public async Task<IEnumerable<Certificate>> ListAsync()
        {
            return await Task.Run(() => _certificates);
        }

        public async Task<Certificate> FindByIdAsync(long id)
        {
            return await Task.Run(() => _certificates.Find(cert => cert.Id == id));
        }

        public async Task CreateAsync(Certificate certificate)
        {
            await Task.Run(() => _certificates.Add(certificate));
        }

        public void Update(Certificate certificate)
        {
            Delete(certificate);
            _certificates.Add(certificate);

        }

        public void Delete(Certificate certificate)
        {
            _certificates.Remove(_certificates.Find(cert => cert.Id == certificate.Id));
        }

    }
}
